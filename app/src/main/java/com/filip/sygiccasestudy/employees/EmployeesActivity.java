package com.filip.sygiccasestudy.employees;

import android.content.DialogInterface;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.design.widget.Snackbar;
import android.view.View;

import com.filip.sygiccasestudy.SygicCaseStudyApplication;
import com.filip.sygiccasestudy.data.model.Employee;
import com.filip.sygiccasestudy.utils.EmployeesAdapter;
import com.filip.sygiccasestudy.R;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

public class EmployeesActivity extends AppCompatActivity implements EmployeesContract.View {

    private static final String STATE_FILTER = "state_filter";

    private RecyclerView mRecyclerView;
    private EmployeesAdapter mEmployeesAdapter;
    private RecyclerView.LayoutManager mLayoutManager;
    private SwipeRefreshLayout mSwipeRefreshLayout;

    @Inject
    EmployeesPresenter mPresenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //injection
        DaggerEmployeesComponent.builder()
                .dataRepositoryComponent(
                        ((SygicCaseStudyApplication)getApplication()).getDataRepositoryComponent())
                .employeesPresenterModule(new EmployeesPresenterModule(this))
                .build()
                .inject(this);

        // Restore filter settings when orientation changed
        if (savedInstanceState != null) {
            mPresenter.setFilter(savedInstanceState.getIntegerArrayList(STATE_FILTER));
        }

        mRecyclerView = (RecyclerView) findViewById(R.id.employeesRecyclerView);
        mRecyclerView.setHasFixedSize(true);
        mLayoutManager = new LinearLayoutManager(this);
        mRecyclerView.setLayoutManager(mLayoutManager);

        mEmployeesAdapter = new EmployeesAdapter(new ArrayList<Employee>(0));
        mRecyclerView.setAdapter(mEmployeesAdapter);

        mSwipeRefreshLayout = (SwipeRefreshLayout) findViewById(R.id.employeeSwipeRefreshLayout);
        mSwipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                mPresenter.onSwipeToRefresh();
            }
        });

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.employeeFabFilter);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mPresenter.onFilterButtonClicked();
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();

        mPresenter.loadEmployees();
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        outState.putIntegerArrayList(STATE_FILTER, mPresenter.getSelectedFilter());

        super.onSaveInstanceState(outState);
    }

    @Override
    public void showEmployees(List<Employee> employees) {
        mEmployeesAdapter.replaceData(employees);
    }

    @Override
    public void showFilterDialog() {
        final ArrayList<Integer> selectedItems = mPresenter.getSelectedFilter();
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle(R.string.filter_dialog_title)
                .setMultiChoiceItems(mPresenter.getFilterArray(), mPresenter.getSelectedFilterAsBool(),
                        new DialogInterface.OnMultiChoiceClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which,
                                                boolean isChecked) {
                                if (isChecked) {
                                    selectedItems.add(which);
                                } else if (selectedItems.contains(which)) {
                                    selectedItems.remove(Integer.valueOf(which));
                                }
                            }
                        })
                .setPositiveButton(R.string.filter_dialog_ok, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int id) {
                        mPresenter.setFilter(selectedItems);
                        mPresenter.onFilterDialogConfirmed();
                    }
                })
                .setCancelable(false);
        builder.create().show();
    }

    @Override
    public void onSwipeToRefreshFinished() {
        if (mSwipeRefreshLayout != null && mSwipeRefreshLayout.isRefreshing()) {
            mSwipeRefreshLayout.setRefreshing(false);
        }
    }

    @Override
    public void showMessageLoadingCompleted() {
        showMessage(getString(R.string.message_loading_complete), Snackbar.LENGTH_SHORT);
    }

    @Override
    public void showMessageLoadingFailed() {
        showMessage(getString(R.string.message_loading_failed), Snackbar.LENGTH_SHORT);
    }

    @Override
    public void showMessageNoSelection() {
        showMessage(getString(R.string.message_no_selection), Snackbar.LENGTH_SHORT);
    }

    private void showMessage(String message, int duration) {
            Snackbar.make(findViewById(R.id.employeeCoordlayout), message, duration).show();
    }
}