package com.filip.sygiccasestudy.employees;


import dagger.Module;
import dagger.Provides;

@Module
public class EmployeesPresenterModule {

    private final EmployeesContract.View mView;

    public EmployeesPresenterModule(EmployeesContract.View view) {
        mView = view;
    }

    @Provides
    EmployeesContract.View provideMonitoringView() {
        return mView;
    }


}
