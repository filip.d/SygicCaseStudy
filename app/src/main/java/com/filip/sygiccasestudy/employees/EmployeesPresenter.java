package com.filip.sygiccasestudy.employees;


import com.filip.sygiccasestudy.data.DataRepository;
import com.filip.sygiccasestudy.data.DataRepositoryContract;
import com.filip.sygiccasestudy.data.model.Employee;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

public class EmployeesPresenter implements EmployeesContract.Presenter {

    // could be loaded from json
    private static final String[] DEPARTMENTS = {"RD", "HR", "FN"};

    private final EmployeesContract.View mEmployeesView;
    private final DataRepository mDataRepository;

    private boolean mIsLoading;

    private ArrayList<Integer> mSelectedFilters = new ArrayList<Integer>();

    @Inject
    EmployeesPresenter(EmployeesContract.View view, DataRepository dataRepository) {
        this.mEmployeesView = view;
        this.mDataRepository = dataRepository;

        // add default filter
        mSelectedFilters.add(0);
    }

    @Override
    public void loadEmployees() {
        if (!mIsLoading && !mSelectedFilters.isEmpty()) {
            mIsLoading = true;
            mDataRepository.updateEmployeesDataFromSource(new DataRepositoryContract.LoadEmployeesCallback() {

                @Override
                public void onEmployeesLoaded(List<Employee> employees) {
                    mDataRepository.refreshLocalRepository(employees);
                    mEmployeesView.showMessageLoadingCompleted();
                    updateView();
                }

                @Override
                public void onDataNotAvailable() {
                    mEmployeesView.showMessageLoadingFailed();
                    updateView();
                }

                private void updateView() {
                    updateEmployeesView();
                    mIsLoading = false;
                    mEmployeesView.onSwipeToRefreshFinished();
                }
            });
        } else {
            updateEmployeesView();
            mEmployeesView.onSwipeToRefreshFinished();
            if (mSelectedFilters.isEmpty()) {
                mEmployeesView.showMessageNoSelection();
            }
        }
    }

    private void updateEmployeesView() {
        List<String> filterArgs = new ArrayList<>();
        for (Integer filter : getSelectedFilter()) {
                filterArgs.add(DEPARTMENTS[filter]);
        }
        mEmployeesView.showEmployees(mDataRepository.getEmployees(filterArgs.toArray(new String[0])));
    }

    @Override
    public void onFilterButtonClicked() {
        mEmployeesView.showFilterDialog();
    }

    @Override
    public void setFilter(ArrayList<Integer> selectedItems) {
        mSelectedFilters = selectedItems;
    }

    @Override
    public CharSequence[] getFilterArray() {
        return DEPARTMENTS;
    }

    @Override
    public ArrayList<Integer> getSelectedFilter() {
        return mSelectedFilters;
    }

    @Override
    public boolean[] getSelectedFilterAsBool() {
        boolean[] boolArray = new boolean[DEPARTMENTS.length];
            for (Integer filter : getSelectedFilter()) {
                boolArray[filter] = true;
            }
        return boolArray;
    }

    @Override
    public void onFilterDialogConfirmed() {
        loadEmployees();
    }

    @Override
    public void onSwipeToRefresh() {
        loadEmployees();
    }
}
