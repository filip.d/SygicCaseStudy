package com.filip.sygiccasestudy.employees;


import com.filip.sygiccasestudy.data.DataRepositoryComponent;
import com.filip.sygiccasestudy.utils.ActivityScoped;

import dagger.Component;

@ActivityScoped
@Component(
        dependencies = {
                DataRepositoryComponent.class

        },
        modules = {
                EmployeesPresenterModule.class,

        }
)
public interface EmployeesComponent {
    void inject(EmployeesActivity activity);
}
