package com.filip.sygiccasestudy.employees;


import com.filip.sygiccasestudy.data.model.Employee;

import java.util.ArrayList;
import java.util.List;

public interface EmployeesContract {

    interface View {
        void showEmployees(List<Employee> employees);
        void showFilterDialog();
        void showMessageLoadingCompleted();
        void showMessageLoadingFailed();
        void showMessageNoSelection();
        void onSwipeToRefreshFinished();
    }

    interface Presenter {
        void loadEmployees();
        void onSwipeToRefresh();
        void onFilterButtonClicked();
        void setFilter(ArrayList<Integer> selectedItems);
        CharSequence[] getFilterArray();
        ArrayList<Integer> getSelectedFilter();
        boolean[] getSelectedFilterAsBool();
        void onFilterDialogConfirmed();
    }
}
