package com.filip.sygiccasestudy.data.local;

import android.content.ContentProvider;
import android.content.ContentUris;
import android.content.ContentValues;
import android.content.UriMatcher;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteQueryBuilder;
import android.net.Uri;
import android.support.annotation.Nullable;
import android.text.TextUtils;

public class EmployeeProvider extends ContentProvider {

    public static final String PROVIDER_NAME = "com.filip.sygiccasestudy.contentprovider";
    public static final Uri CONTENT_URI = Uri.parse("content://"+ PROVIDER_NAME + "/employees");

    public static final String TABLE_EMPLOYEES = "employees";

    public static final String COLUMN_ID = "_id";
    public static final String COLUMN_FIRST_NAME = "first_name";
    public static final String COLUMN_LAST_NAME = "last_name";
    public static final String COLUMN_ICON_URL = "icon_url";
    public static final String COLUMN_DEPARTMENT = "department";

    private static final int EMPLOYEES = 1;
    private static final int DEPARTMENT_EMPLOYEES = 2;
    private static final int DEPARTMENT_EMPLOYEE_ID = 3;

    private static final UriMatcher uriMatcher;
    static {
        uriMatcher = new UriMatcher(UriMatcher.NO_MATCH);
        uriMatcher.addURI(PROVIDER_NAME, "employees", EMPLOYEES);
        uriMatcher.addURI(PROVIDER_NAME, "employees/*", DEPARTMENT_EMPLOYEES);
        uriMatcher.addURI(PROVIDER_NAME, "employees/*/#", DEPARTMENT_EMPLOYEE_ID);
    }

    private SQLiteDatabase mEmployeesDb;

    @Override
    public boolean onCreate() {
        SqlHelper sqlHelper = new SqlHelper(getContext());
        mEmployeesDb = sqlHelper.getWritableDatabase();

        if (mEmployeesDb == null) {
            return false;
        }
        return true;
    }

    @Nullable
    @Override
    public Cursor query(Uri uri, String[] projection, String selection, String[] selectionArgs, String sortOrder) {
        SQLiteQueryBuilder sqlBuilder = new SQLiteQueryBuilder();
        sqlBuilder.setTables(TABLE_EMPLOYEES);

        switch (uriMatcher.match(uri)) {
            case EMPLOYEES:
                if (TextUtils.isEmpty(sortOrder)) sortOrder = COLUMN_ID + " ASC";
                break;
            case DEPARTMENT_EMPLOYEES:
                sqlBuilder.appendWhere(
                        COLUMN_DEPARTMENT +
                        " = \'" + uri.getPathSegments().get(1) + "\'"
                );
                break;
            case DEPARTMENT_EMPLOYEE_ID:
                sqlBuilder.appendWhere(
                        COLUMN_DEPARTMENT +
                                " = \'" + uri.getPathSegments().get(1) + "\'"
                );
                sqlBuilder.appendWhere(
                        COLUMN_ID +
                                " = " +
                                uri.getPathSegments().get(2)
                );
        }

        Cursor c = sqlBuilder.query(mEmployeesDb, projection, selection, selectionArgs, null, null, sortOrder);
        c.setNotificationUri(getContext().getContentResolver(), uri);
        return c;
    }

    @Nullable
    @Override
    public String getType(Uri uri) {
        return null;
    }

    @Nullable
    @Override
    public Uri insert(Uri uri, ContentValues contentValues) {
        long id = mEmployeesDb.insert(TABLE_EMPLOYEES, "", contentValues);

        if (id > 0) {
        Uri _uri = ContentUris.withAppendedId(CONTENT_URI, id);
            getContext().getContentResolver().notifyChange(_uri, null);
            return _uri;
        }
        throw new UnsupportedOperationException("Failed to insert row into " + uri);
    }

    @Override
    public int delete(Uri uri, String selection, String[] selectionArgs) {
        int count = 0;

        switch (uriMatcher.match(uri)) {
            case EMPLOYEES:
                count = mEmployeesDb.delete(TABLE_EMPLOYEES, selection, selectionArgs);
                break;
            case DEPARTMENT_EMPLOYEES:
                count = mEmployeesDb.delete(
                        TABLE_EMPLOYEES,
                        COLUMN_DEPARTMENT +  " = \'" + uri.getPathSegments().get(1) + "\'" +
                                (!TextUtils.isEmpty(selection) ? " AND (" + selection + ')' : ""),
                        selectionArgs);
                break;
            case DEPARTMENT_EMPLOYEE_ID:
                count = mEmployeesDb.delete(
                        TABLE_EMPLOYEES,
                        COLUMN_DEPARTMENT + " = \'" + uri.getPathSegments().get(1) + "\'" +
                        " AND " +
                        COLUMN_ID + " = " + uri.getPathSegments().get(2) +
                        (!TextUtils.isEmpty(selection) ? " AND (" + selection + ')' : ""),
                        selectionArgs);
                break;
             default:
                 throw new UnsupportedOperationException("Failed to delete. Invalid URI " + uri);
        }

        getContext().getContentResolver().notifyChange(uri, null);
        return count;
    }

    @Override
    public int update(Uri uri, ContentValues contentValues, String selection, String[] selectionArgs) {
        int count = 0;

        switch (uriMatcher.match(uri)) {
            case EMPLOYEES:
                count = mEmployeesDb.update(TABLE_EMPLOYEES, contentValues, selection, selectionArgs);
                break;
            case DEPARTMENT_EMPLOYEES:
                count = mEmployeesDb.update(
                        TABLE_EMPLOYEES,
                        contentValues,
                        COLUMN_DEPARTMENT + " = \'" + uri.getPathSegments().get(1) + "\'" +
                                (!TextUtils.isEmpty(selection) ? " AND (" + selection + ')' : ""),
                        selectionArgs);
                break;
            case DEPARTMENT_EMPLOYEE_ID:
                count = mEmployeesDb.update(
                        TABLE_EMPLOYEES,
                        contentValues,
                        COLUMN_DEPARTMENT + " = \'" + uri.getPathSegments().get(1) + "\'" +
                                " AND " +
                                COLUMN_ID + " = " + uri.getPathSegments().get(2) +
                                (!TextUtils.isEmpty(selection) ? " AND (" + selection + ')' : ""),
                        selectionArgs);
                break;
            default:
                throw new UnsupportedOperationException("Failed to update. Invalid URI " + uri);
        }

        getContext().getContentResolver().notifyChange(uri, null);
        return count;
    }

}
