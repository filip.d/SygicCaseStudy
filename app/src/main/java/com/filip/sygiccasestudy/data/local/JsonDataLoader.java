package com.filip.sygiccasestudy.data.local;

import android.content.Context;
import android.os.AsyncTask;

import com.filip.sygiccasestudy.data.DataRepositoryContract;
import com.filip.sygiccasestudy.data.model.Employee;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

public class JsonDataLoader extends AsyncTask<Void, Void, List<Employee>> {

    private static final String JSON_ROOT = "Departments";
    private static final String JSON_DEPARTMENT = "Name";
    private static final String JSON_EMPLOYEES = "employees";
    private static final String JSON_FIRST_NAME = "firstName";
    private static final String JSON_LAST_NAME = "lastName";
    private static final String JSON_ICON_URL = "avatar";

    private DataRepositoryContract.LoadEmployeesCallback mCallback;
    private Context mContext;
    private String mJsonFilename;

    public JsonDataLoader(DataRepositoryContract.LoadEmployeesCallback callback, Context context, String jsonFileName) {
        this.mCallback = callback;
        this.mContext = context;
        this.mJsonFilename = jsonFileName;
    }

    private String loadJSONFromAsset() {
        String json;
        try {
            InputStream is = mContext.getAssets().open(mJsonFilename);
            int size = is.available();
            byte[] buffer = new byte[size];
            is.read(buffer);
            is.close();
            json = new String(buffer, "UTF-8");
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
        return json;
    }

    @Override
    protected List<Employee> doInBackground(Void... params) {
        List<Employee> employeesList = new ArrayList<>();

        try {
            String json = loadJSONFromAsset();
            if (json == null) {
                return null;
            }

            JSONObject jObject = new JSONObject(json);
            JSONArray departments = jObject.getJSONArray(JSON_ROOT);

            for (int i = 0; i < departments.length(); ++i) {
                JSONObject department = departments.getJSONObject(i);
                String departmentName = department.getString(JSON_DEPARTMENT);
                JSONArray employees = department.getJSONArray(JSON_EMPLOYEES);
                for (int j = 0; j < employees.length(); ++j) {
                    JSONObject employee = employees.getJSONObject(j);
                    String firstName = employee.getString(JSON_FIRST_NAME);
                    String lastName = employee.getString(JSON_LAST_NAME);
                    String iconUrl = employee.getString(JSON_ICON_URL);

                    Employee e = new Employee(firstName, lastName, iconUrl, departmentName);
                    employeesList.add(e);
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
            return null;
        }

        return employeesList;
    }

    protected void onPostExecute(List<Employee> employees) {
        if (employees != null) {
            mCallback.onEmployeesLoaded(employees);
        } else {
            mCallback.onDataNotAvailable();
        }
    }
}
