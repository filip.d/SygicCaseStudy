package com.filip.sygiccasestudy.data;


import com.filip.sygiccasestudy.data.model.Employee;

import java.util.List;

public interface DataRepositoryContract {

    interface LoadEmployeesCallback {
        void onEmployeesLoaded(List<Employee> machines);
        void onDataNotAvailable();
    }

    List<Employee> getEmployees(String... departments);
    void updateEmployeesDataFromSource(LoadEmployeesCallback loadEmployeesCallback);
    void refreshLocalRepository(List<Employee> employees);

}
