package com.filip.sygiccasestudy.data;

import android.content.ContentResolver;
import android.content.Context;


import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

@Module
public class DataRepositoryModule {

    private Context mContext;

    public DataRepositoryModule(Context context) {
        this.mContext = context;
    }

    @Provides
    @Singleton
    ContentResolver provideContentResolver() {
        return mContext.getContentResolver();
    }



}
