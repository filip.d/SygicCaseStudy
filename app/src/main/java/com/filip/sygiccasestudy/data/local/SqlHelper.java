package com.filip.sygiccasestudy.data.local;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class SqlHelper extends SQLiteOpenHelper
{
    public static final int DATABASE_VERSION = 1;
    public static final String DATABASE_NAME = "employees.db";

    public SqlHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db)
    {
        String query =
                "CREATE TABLE " + EmployeeProvider.TABLE_EMPLOYEES +
                        "(" +
                        EmployeeProvider.COLUMN_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
                        EmployeeProvider.COLUMN_FIRST_NAME + " TEXT, " +
                        EmployeeProvider.COLUMN_LAST_NAME + " TEXT, " +
                        EmployeeProvider.COLUMN_ICON_URL + " TEXT," +
                        EmployeeProvider.COLUMN_DEPARTMENT + " TEXT" +
                        ");";
        db.execSQL(query);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS " + EmployeeProvider.TABLE_EMPLOYEES);
        onCreate(db);
    }
}
