package com.filip.sygiccasestudy.data;

import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.net.Uri;

import com.filip.sygiccasestudy.data.local.EmployeeProvider;
import com.filip.sygiccasestudy.data.local.JsonDataLoader;
import com.filip.sygiccasestudy.data.model.Employee;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;
import javax.inject.Singleton;

@Singleton
public class DataRepository implements DataRepositoryContract {

    private static final String JSON_DATA_FILENAME = "jsonDataFile.json";
    private static final String URI_ALL_EMPLOYEES = "content://com.filip.sygiccasestudy.contentprovider/employees";

    private ContentResolver mContentResolver;
    private Context mContext;

    @Inject
    public DataRepository(Context context, ContentResolver contentResolver) {
        this.mContext = context;
        this.mContentResolver = contentResolver;
    }

    @Override
    public List<Employee> getEmployees(String... departments) {

        List<Employee> employees = new ArrayList<>();

        if (departments.length == 0) {
            return employees;
        }

        for (String department : departments) {
            Uri uri = Uri.parse(URI_ALL_EMPLOYEES + "/" + department);
            Cursor c = mContentResolver.query(uri, null, null, null, null);
            if (c != null && c.moveToFirst()) {
                do {
                    employees.add(
                            new Employee(
                                    c.getString(c.getColumnIndex(EmployeeProvider.COLUMN_FIRST_NAME)),
                                    c.getString(c.getColumnIndex(EmployeeProvider.COLUMN_LAST_NAME)),
                                    c.getString(c.getColumnIndex(EmployeeProvider.COLUMN_ICON_URL)),
                                    c.getString(c.getColumnIndex(EmployeeProvider.COLUMN_DEPARTMENT))
                            )
                    );
                } while (c.moveToNext());
                c.close();
            }
        }
        return employees;
    }


    @Override
    public void updateEmployeesDataFromSource(final LoadEmployeesCallback callback) {
        if (callback == null) {
            return;
        }

        // Load and parse a JSON file asynchronously
        JsonDataLoader jsonDataLoader = new JsonDataLoader(callback, mContext, JSON_DATA_FILENAME);
        jsonDataLoader.execute();
    }

    @Override
    public void refreshLocalRepository(List<Employee> employees) {
        // implement update instead of deleting all database?
        Uri allEmployees = Uri.parse(URI_ALL_EMPLOYEES);
        mContentResolver.delete(allEmployees, null, null);

        for (Employee e : employees) {
            ContentValues values = new ContentValues();
            values.put(EmployeeProvider.COLUMN_FIRST_NAME, e.getFirstName());
            values.put(EmployeeProvider.COLUMN_LAST_NAME, e.getLastName());
            values.put(EmployeeProvider.COLUMN_ICON_URL, e.getIconUrl());
            values.put(EmployeeProvider.COLUMN_DEPARTMENT, e.getDeparment());
            mContentResolver.insert(EmployeeProvider.CONTENT_URI, values);
        }
    }

}
