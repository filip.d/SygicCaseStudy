package com.filip.sygiccasestudy.data;


import com.filip.sygiccasestudy.ApplicationModule;

import javax.inject.Singleton;

import dagger.Component;

@Singleton
@Component(modules =
        {
                DataRepositoryModule.class,
                ApplicationModule.class,
        }

)
public interface DataRepositoryComponent {
    DataRepository getDataRepository();
}