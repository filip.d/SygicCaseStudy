package com.filip.sygiccasestudy.data.model;

public class Employee {

    private String mFirstName;
    private String mLastName;
    private String mIconUrl;
    private String mDeparment;

    public Employee() {

    }

    public Employee(String firstName, String lastName, String iconUrl, String deparment) {
        this.mFirstName = firstName;
        this.mLastName = lastName;
        this.mIconUrl = iconUrl;
        this.mDeparment = deparment;
    }

    public String getFirstName() {
        return mFirstName;
    }

    public void setFirstName(String firstName) {
        this.mFirstName = firstName;
    }

    public String getLastName() {
        return mLastName;
    }

    public void setLastName(String lastName) {
        this.mLastName = lastName;
    }

    public String getIconUrl() {
        return mIconUrl;
    }

    public void setIcon(String iconUrl) {
        this.mIconUrl = iconUrl;
    }

    public String getDeparment() {
        return mDeparment;
    }

    public void setDeparment(String deparment) {
        this.mDeparment = deparment;
    }
}
