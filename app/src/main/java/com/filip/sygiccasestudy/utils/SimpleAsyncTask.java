package com.filip.sygiccasestudy.utils;

import android.app.Activity;

public abstract class SimpleAsyncTask<T> {

    private T mResult;

    protected abstract void onPreExecute();

    protected abstract T doInBackground();

    protected abstract void onPostExecute(T result);

    public void execute(final Activity activity) {

        onPreExecute();

        new Thread(new Runnable() {
            @Override
            public void run() {
                mResult = doInBackground();
                activity.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        onPostExecute(mResult);
                    }
                });
            }
        }).start();
    }
}
