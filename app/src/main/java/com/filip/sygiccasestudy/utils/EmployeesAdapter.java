package com.filip.sygiccasestudy.utils;


import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.filip.sygiccasestudy.R;
import com.filip.sygiccasestudy.data.model.Employee;
import com.squareup.picasso.Picasso;

import java.util.List;

public class EmployeesAdapter extends RecyclerView.Adapter<EmployeesAdapter.ViewHolder> {
    List<Employee> mEmployees;

    public static class ViewHolder extends RecyclerView.ViewHolder {
        public CardView mEmployeeCardView;
        public ViewHolder(CardView v) {
            super(v);
            mEmployeeCardView = v;
        }
    }

    public EmployeesAdapter(List<Employee> employees) {
        setList(employees);
    }

    public void replaceData(List<Employee> employees) {
        setList(employees);
        notifyDataSetChanged();
    }

    private void setList(List<Employee> employees) {
        if (employees == null)
            throw new NullPointerException();
        else
            mEmployees = employees;
    }

    @Override
    public EmployeesAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.cardview_employe, parent, false);

        ViewHolder vh = new ViewHolder((CardView)v);
        return vh;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        TextView lastNameTextView =
                (TextView) holder.mEmployeeCardView.findViewById(R.id.lastNameTextView);
        TextView firstNameTextView =
                (TextView) holder.mEmployeeCardView.findViewById(R.id.firstNameTextView);
        ImageView iconImageView =
                (ImageView) holder.mEmployeeCardView.findViewById(R.id.employeeIconImageView);

        lastNameTextView.setText(mEmployees.get(position).getLastName());
        firstNameTextView.setText(mEmployees.get(position).getFirstName());

        Picasso.with(holder.mEmployeeCardView.getContext())
                .load(mEmployees.get(position).getIconUrl())
                .placeholder(R.drawable.ic_person_primarydark_64dp)
                .error(R.drawable.ic_person_primarydark_64dp)
                .into(iconImageView);
    }

    @Override
    public int getItemCount() {
        return mEmployees.size();
    }
}



