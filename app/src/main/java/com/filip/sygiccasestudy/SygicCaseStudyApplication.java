package com.filip.sygiccasestudy;

import android.app.Application;

import com.filip.sygiccasestudy.data.DaggerDataRepositoryComponent;
import com.filip.sygiccasestudy.data.DataRepositoryComponent;
import com.filip.sygiccasestudy.data.DataRepositoryModule;


public class SygicCaseStudyApplication extends Application {

    private DataRepositoryComponent mDataRepositoryComponent;

    @Override
    public void onCreate() {
        super.onCreate();

        mDataRepositoryComponent = DaggerDataRepositoryComponent.builder()
                .applicationModule(new ApplicationModule(getApplicationContext()))
                .dataRepositoryModule(new DataRepositoryModule(getApplicationContext()))
                .build();
    }

    public DataRepositoryComponent getDataRepositoryComponent() {
        return mDataRepositoryComponent;
    }
}
