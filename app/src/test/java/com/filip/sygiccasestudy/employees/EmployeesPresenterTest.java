package com.filip.sygiccasestudy.employees;

import com.filip.sygiccasestudy.data.DataRepository;
import com.filip.sygiccasestudy.data.DataRepositoryContract;
import com.filip.sygiccasestudy.data.model.Employee;

import org.junit.Before;
import org.junit.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.util.ArrayList;
import java.util.List;

import static junit.framework.Assert.assertTrue;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;


public class EmployeesPresenterTest {

    @Mock
    DataRepository mDataRepository;

    @Mock
    EmployeesContract.View mEmployeesView;

    @Captor
    private ArgumentCaptor<DataRepositoryContract.LoadEmployeesCallback> mLoadEmployeesCallbackCaptor;

    @Captor
    private ArgumentCaptor<List<Employee>> mShowEmployeesArgumentCaptor;

    private EmployeesPresenter mEmployeesPresenter;

    private static List<Employee> sEmployees;

    @Before
    public void setupEmployeePresenter() {

        MockitoAnnotations.initMocks(this);

        mEmployeesPresenter = new EmployeesPresenter(mEmployeesView, mDataRepository);

        sEmployees = new ArrayList<Employee>();
        sEmployees.add(new Employee("Darth", "Vader", "iconUrl", "RD"));
        sEmployees.add(new Employee("Honzik", "Hloupy", "iconUrl", "RD"));
        sEmployees.add(new Employee("Stara", "Tvoje", "iconUrl", "HR"));
        sEmployees.add(new Employee("Harddick", "BJ", "iconUrl", "FN"));

    }

    @Test
    public void loadEmployeesTest_DataAvailable() {
        when(mDataRepository.getEmployees("RD")).thenReturn(sEmployees);

        // Call tested method
        mEmployeesPresenter.loadEmployees();

        // Callback is captured and invoked with sEmployees
        verify(mDataRepository).updateEmployeesDataFromSource(mLoadEmployeesCallbackCaptor.capture());
        mLoadEmployeesCallbackCaptor.getValue().onEmployeesLoaded(sEmployees);

        // These three methods shall be called
        verify(mEmployeesView).showMessageLoadingCompleted();
        verify(mEmployeesView).onSwipeToRefreshFinished();
        verify(mDataRepository).refreshLocalRepository(sEmployees);

        // Filtering is not working, it's overridden by when statement
        verify(mDataRepository).getEmployees("RD");

        // All employees from sEmployees shall be shown
        ArgumentCaptor<List> showEmployeesArgumentCaptor = ArgumentCaptor.forClass(List.class);
        verify(mEmployeesView).showEmployees(showEmployeesArgumentCaptor.capture());
        assertTrue(showEmployeesArgumentCaptor.getValue().size() == 4);
    }

    @Test
    public void loadEmployeesTest_DataNotAvailable() {
        when(mDataRepository.getEmployees("RD")).thenReturn(sEmployees);

        mEmployeesPresenter.loadEmployees();

        verify(mDataRepository).updateEmployeesDataFromSource(mLoadEmployeesCallbackCaptor.capture());
        mLoadEmployeesCallbackCaptor.getValue().onDataNotAvailable();

        verify(mEmployeesView).showMessageLoadingFailed();
        verify(mDataRepository).getEmployees("RD");

        ArgumentCaptor<List> showEmployeesArgumentCaptor = ArgumentCaptor.forClass(List.class);
        verify(mEmployeesView).showEmployees(showEmployeesArgumentCaptor.capture());
        assertTrue(showEmployeesArgumentCaptor.getValue().size() == 4);
    }
}